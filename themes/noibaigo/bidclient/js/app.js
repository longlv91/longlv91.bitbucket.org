// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'timer', 'emguo.poller', 'ngResource'/*, 'ngCordova'*/])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        console.log(ionic.Platform.device().uuid);
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            cache: false,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })
        .state('app.bidlist', {
            url: '/bidlist',
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'templates/bidlist.html',
                    controller: 'BidlistsCtrl'
                }
            }
        })
        .state('app.login', {
            url: '/login',
            views: {
                'menuContent': {
                    templateUrl: 'templates/login.html',
                    controller: 'LoginCtrl'
                }
            }
        })

    .state('app.winlist', {
        url: '/winlist',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/winlist.html',
                controller: 'WinListCtrl'
            }
        }
    })

    .state('app.message', {
        url: '/message',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/message.html',
                controller: 'MessageListCtrl'
            }
        }
    })

    .state('app.topuplist', {
        url: '/topuplist',
        views: {
            'menuContent': {
                templateUrl: 'templates/topuplist.html',
                controller: 'TopupListCtrl'
            }
        }
    })

    .state('app.singlemessage', {
        url: '/messagelist/:messageId',
        views: {
            'menuContent': {
                templateUrl: 'templates/messagedetail.html',
                controller: 'MessageDetailCtrl'
            }
        }
    })

    .state('app.joinlist', {
        url: '/joinlist',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/joinlist.html',
                controller: 'JoinListCtrl'
            }
        }
    })

    .state('app.single', {
        url: '/bidlist/:routeId',
        views: {
            'menuContent': {
                templateUrl: 'templates/biddetail.html',
                controller: 'DetailCtrl'
            }
        }
    })

    .state('app.help', {
        url: '/help',
        views: {
            'menuContent': {
                templateUrl: 'templates/help.html'
            }
        }
    })

    .state('app.setting', {
        url: '/setting',
        views: {
            'menuContent': {
                templateUrl: 'templates/setting.html',
                controller: 'SettingCtrl'
            }
        }
    });


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
