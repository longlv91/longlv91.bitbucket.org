angular.module('starter.controllers', [])

.factory('tokenStore', function() {
    var token = username = '';
    if(typeof(Storage) !== 'undefined'){
        token = localStorage.token;
        username = localStorage.username;
    }
    return {
        getToken: function() {
            return token;
        },
        getUsenamer: function() {
            return username;
        },
        setToken: function(newToken) {
            token = newToken;
        },
        setUsername: function(newUsername) {
            username = newUsername;
        }
    }
})

.factory('getBidList', function($resource, tokenStore) {
    return {
        getResource: function(params) {
            return $resource('https://www.noibaiconnect.com/bidcore/query.php', {
                action: 'getBidList',
                username: tokenStore.getUsenamer(),
                token: tokenStore.getToken(),
                params: params,
                callback: 'JSON_CALLBACK'

            }, {
                jsonp_get: {
                    method: 'JSONP'
                }
            });
        }
    }
})

.factory('BidListService', function($q, poller, getBidList) {
    var routes = [];
    // var myPoller;
    return {
        getData: function(params, callback) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            myPoller = poller.get(getBidList.getResource(params), {
                action: 'jsonp_get',
                delay: 5000
            });
            myPoller.promise.then(null, null, function(response) {
                if (response.success == 1) {
                    routes = (response);
                    callback(routes);
                } else {
                    deferred.reject(response.detail);
                }
            });

            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        },
        stopPoller: function() {
            myPoller.stop();
        },
        find: function(routeId, callback) {
            var route = routes.data.filter(function(entry) {
                return entry.id == routeId;
            })[0];
            callback(route);
        }
    }
})

.service('LoginService', function($q, $http, $rootScope) {
    return {
        loginUser: function(name, pw) {

            var deferred = $q.defer();
            var promise = deferred.promise;

   //          /////////kiểm tra fingerprint
   //          var client = new ClientJS(); // Create A New Client Object
   //          var canvasPrint = client.getCanvasPrint();

			// var fingerprint = client.getCustomFingerprint(1, canvasPrint);
			// // alert(fingerprint1);

   //          // var fingerprint = client.getFingerprint(); // Calculate Device/Browser Fingerprint

   //          var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getFingerprint&username=0" + name + "&password=" + pw + "&fingerprint=" + fingerprint + "&callback=JSON_CALLBACK";

   //          $http.jsonp(url).
   //          success(function(data, status, headers, config) {
   //              if (data.success != 1){
   //                  deferred.reject(data.detail);
   //              }
   //          }).
   //          error(function(data, status, headers, config) {
   //              deferred.reject('Thiết bị của bạn đang không được kết nối internet hoặc 3G!');
   //          });

   //          promise.success = function(fn) {
   //              promise.then(fn);
   //              return promise;
   //          }
   //          promise.error = function(fn) {
   //              promise.then(null, fn);
   //              return promise;
   //          }

            //////////////////////kiểm tra login

            var deferred = $q.defer();
            var promise = deferred.promise;

            var url = "https://www.noibaiconnect.com/bidcore/query.php?action=login&ver=1&username=0" + name + "&password=" + pw + "&callback=JSON_CALLBACK";

            $http.jsonp(url).
            success(function(data, status, headers, config) {
                if (data.success == 1) {
                    $rootScope.fullname = data.fullname;
                    deferred.resolve(data);
                } else
                    deferred.reject(data.detail);
            }).
            error(function(data, status, headers, config) {
                deferred.reject('Thiết bị của bạn đang không được kết nối internet hoặc 3G!');
            });

            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})

.controller('AppCtrl', function($scope, $ionicPopup, $window, $location, $rootScope, $interval) {

    $interval(function() {
        $rootScope.curtime = new Date();
    }, 1000);

    $rootScope.currentCredit = 0;
    $rootScope.seatNo = 0;

    $scope.Logout = function() {

        var confirmPopup = $ionicPopup.confirm({
            title: 'Thoát',
            template: 'Bạn có chắc muốn thoát khỏi chương trình?'
        });
        confirmPopup.then(function(res) {
            if (res) {
                localStorage.removeItem('token');
                localStorage.removeItem('username');
                $location.path('/login');
                $scope.$apply();
                $window.location.reload();
            }
        });
    }

    $scope.timerRunning = true;
    $scope.$broadcast('timer-start');

})

.controller('LoginCtrl', function($scope, $ionicSideMenuDelegate, $state, LoginService, tokenStore, $ionicPopup, $ionicLoading, $http, $ionicActionSheet, $timeout) {

    if(typeof(Storage) !== 'undefined' && localStorage.token != undefined){
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=isExistToken&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $state.go('app.bidlist');
                return;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
                localStorage.removeItem('token');
                localStorage.removeItem('username');
            }
        }).
        error(function(data, status, headers, config) {
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
            localStorage.removeItem('token');
                localStorage.removeItem('username');
        });
    }

    // Form data for the login modal
    $scope.loginData = {};
    $ionicSideMenuDelegate.canDragContent(false)
        // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        LoginService.loginUser($scope.loginData.username, $scope.loginData.password).success(function(data) {
            $ionicLoading.hide();
            tokenStore.setToken(data.token);
            tokenStore.setUsername(data.username);
            ///////////////////
            if(typeof(Storage) !== 'undefined'){
                localStorage.token = data.token;
                localStorage.username = data.username;
            }
            ///////////////////
            $state.go('app.bidlist');
            $scope.loginData = {};
        }).error(function(data) {
            $ionicLoading.hide();
            $scope.loginData.password = '';
            var alertPopup = $ionicPopup.alert({
                title: 'Đăng nhập thất bại',
                template: data
            });
            localStorage.removeItem('token');
            localStorage.removeItem('username');
        });
    };

    $scope.showHelp = function() {

        // Show the action sheet
        var hideSheet = $ionicActionSheet.show({
            buttons: [
                { text: 'Quên Mật Khẩu' },
                { text: 'Muốn Trở Thành Lái Xe?' }
            ],
            titleText: 'Noibai Connect System',
            cancelText: 'Đóng',
            cancel: function() {
                //...
            },
            buttonClicked: function(index) {
                return true;
            }
        });

        $timeout(function() {
            hideSheet();
        }, 4000);

    };

})

.controller('BidlistsCtrl', function($scope, $state, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $ionicPopup, BidListService, $location, $window, $ionicLoading, tokenStore, $http/*, $cordovaLocalNotification*/) {

        // $cordovaLocalNotification.schedule({
        //     id: 1,
        //     title: 'Thử nghiệm title',
        //     text: 'Thử nghiệm nội dung',
        //     every: 'minute'
        // }).then(function (result) {
        //     alert('xong');
        // });

        $scope.routes = new Array();
        if ($scope.routes.length <= 0) {
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
        }
        $scope.allFilter = {"mybid": false, "today" : false, "toAirport": true, "fromAirport": true, "otherAirport": true, "bid": true, "buy": true, "other": true};
        var params = $scope.allFilter;

        BidListService.getData(params, function(data) {
            $rootScope.currentCredit = data.current_credit;
            $rootScope.seatNo = data.seat_no;
            document.title = '(' + $scope.formatCurrency(data.current_credit) + ') - ' + $rootScope.fullname + ' | Noibai Connect';
            $rootScope.messageCount = data.message_count;
            $scope.message_title = data.message_title;
            var data = data.data;
            //thêm nếu chưa có gì
            // if ($scope.routes.length <= 0) {
            //     for (i = 0; i < data.length; i++) {
            //         $scope.routes.push(data[i]);
            //     }
            //     $ionicScrollDelegate.resize();
            //     $ionicLoading.hide();
            // } else {
            //     //xóa các route dư thừa
            //     for (i = 0; i < $scope.routes.length; i++) {
            //         var found = false;
            //         for (j = 0; j < data.length; j++) {
            //             if ($scope.routes[i]['id'] == data[j]['id']) {
            //                 found = true;
            //                 break;
            //             }
            //         }
            //         if (!found) {
            //             $rootScope.justAdd = 0;
            //             $scope.routes.splice(i, 1);
            //             $ionicScrollDelegate.resize();
            //         }
            //     }
            //     var countAdd = 0;
            //     //tìm lọc routes và push hoặc change các route bị thay đổi
            //     for (i = 0; i < data.length; i++) {
            //         var found = false;
            //         for (j = 0; j < $scope.routes.length; j++) {
            //             if ($scope.routes[j]['id'] == data[i]['id']) {
            //                 $scope.routes[j]['start'] = data[i]['start'];
            //                 $scope.routes[j]['destination'] = data[i]['destination'];
            //                 $scope.routes[j]['pickup_datetime'] = data[i]['pickup_datetime'];
            //                 $scope.routes[j]['seats'] = data[i]['seats'];
            //                 $scope.routes[j]['receipt'] = data[i]['receipt'];
            //                 $scope.routes[j]['buy_price'] = data[i]['buy_price'];
            //                 $scope.routes[j]['start_price'] = data[i]['start_price'];
            //                 $scope.routes[j]['current_price'] = data[i]['current_price'];
            //                 $scope.routes[j]['your_price'] = data[i]['your_price'];
            //                 $scope.routes[j]['end_datetime'] = data[i]['end_datetime'];
            //                 $scope.routes[j]['is_round_trip'] = data[i]['is_round_trip'];
            //                 $scope.routes[j]['biduserlist'] = data[i]['biduserlist'];
            //                 $scope.routes[j]['is_outbid'] = data[i]['is_outbid'];
            //                 found = true;
            //                 break;
            //             }
            //         }
            //         if (!found) {
            //             countAdd++;
            //             $scope.routes.push(data[i]);
            //             $ionicScrollDelegate.resize();
            //         }
            //         if (countAdd > 0)
            //             $rootScope.justAdd = countAdd;
            //     }
            // }
            $ionicLoading.hide();
            $scope.routes = data;
        }).error(function(data) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                template: data
            });
            alertPopup.then(function(res) {
                $location.path('/login');
                $scope.$apply();
                $window.location.reload();
            });
        });

        $scope.viewMessage = function(){
            $state.go('app.message');
        }

        $scope.clearSearch = function() {
            $scope.searchFilter = '';
        };

        $scope.listCanSwipe = true;

        $scope.formatDate = function(date) {
            var userAgent = navigator.userAgent;
            var browser = 'chrome';
            var browsers = {
                chrome: /chrome/i,
                safari: /safari/i,
                firefox: /firefox/i,
                ie: /internet explorer/i
            };

            for (var key in browsers) {
                if (browsers[key].test(userAgent)) {
                    browser = key;
                }
            }

            if (ionic.Platform.isIOS() || browser == 'safari') {
                date = date.replace(' ', 'T');
            }

            if (ionic.Platform.isAndroid() || browser == 'chrome') {
                date = date.replace(' ', 'T');
                date += 'Z';
            }

            var dateOut = new Date(date);
            dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
            return dateOut;
        };

        $scope.returnMilliseconds = function(date) {

            var userAgent = navigator.userAgent;
            var browser = 'chrome';
            var browsers = {
                chrome: /chrome/i,
                safari: /safari/i,
                firefox: /firefox/i,
                ie: /internet explorer/i
            };

            for (var key in browsers) {
                if (browsers[key].test(userAgent)) {
                    browser = key;
                }
            }

            if (ionic.Platform.isIOS() || browser == 'safari') {
                date = date.replace(' ', 'T');
            }

            if (ionic.Platform.isAndroid() || browser == 'chrome') {
                date = date.replace(' ', 'T');
                date += 'Z';
            }

            var dateOut = new Date(date);
            dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
            return dateOut.getTime();
        };

        $scope.formatCurrency = function(price) {
            return (price / 1000).toLocaleString() + 'k';
        };

        $scope.buy = function(route) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Xác nhận',
                template: 'Bạn có chắc muốn mua ngay lịch này với giá <b>' + $scope.formatCurrency(route.buy_price) + '</b>?'
            });
            confirmPopup.then(function(res) {
                if (res) {
                    //Nếu đồng ý Mua
                    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=buy&price="+route.buy_price+"&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                    $http.jsonp(url).
                    success(function(data, status, headers, config) {
                        if (data.success == 1) {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Thông báo',
                                template: 'Bạn đã mua <b>thành công</b>!'
                            });
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Lỗi',
                                template: data.detail
                            });
                        }
                    }).
                    error(function(data, status, headers, config) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Lỗi',
                            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                        });
                    });
                }
            });

        };

        $scope.bid = function(route) {
            var buyPopup = $ionicPopup.show({
                template: '<input type="number" ng-model="bid.price">',
                title: 'Nhập giá mà bạn muốn đi',
                subTitle: 'Ví dụ: Nếu muốn đi với giá 350.000đ xin nhập 350 vào ô dưới.<br> Giá hiện tại đang là <b>'+$scope.formatCurrency(route.current_price)+'</b>',
                scope: $scope,
                buttons: [{
                    text: 'Hủy'
                }, {
                    text: '<b>Đồng Ý</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        // if (!$scope.bid.price) {
                        //     //don't allow the user to close unless he enters bid price
                        //     e.preventDefault();
                        // } else {
                            return $scope.bid.price;
                        // }
                    }
                }, ]
            });
            buyPopup.then(function(res) {
                if (res) {
                    if (route.current_price - (res*1000) < 10000 || (route.current_price - (res*1000))%10000 != 0) {
                        $ionicPopup.alert({
                            title: 'Lỗi',
                            template: 'Số tiền trả giá phải nhỏ hơn <b>'+$scope.formatCurrency(route.current_price)+'</b> và chẵn <b>10k</b>!'
                        });
                        return;
                    }
                    // if (res >= route.current_price + 10000) {
                    //     $ionicPopup.alert({
                    //         title: 'Lỗi',
                    //         template: 'Bước trả giá phải lớn hơn 10k!'
                    //     });
                    //     return;
                    // }
                    //Nếu đồng ý Mua
                    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=bid&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&price=" + res + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                    $http.jsonp(url).
                    success(function(data, status, headers, config) {
                        if (data.success == 1) {
                            $ionicPopup.alert({
                                title: 'Thông báo',
                                template: 'Bạn đã trả giá <b>hợp lệ</b>!'
                            });
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Lỗi',
                                template: data.detail
                            });
                        }
                    }).
                    error(function(data, status, headers, config) {
                        $ionicPopup.alert({
                            title: 'Lỗi',
                            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                        });
                    });
                }
            });
        };

        $scope.priceinf = function(route) {
            var pricePopup = $ionicPopup.show({
                template: '<input type="number" ng-model="bid.priceinf">',
                title: 'Nhập giá mà bạn muốn nhận cuốc này',
                subTitle: 'Giá bạn đưa ra sẽ được dùng để thương lượng với KH. Nếu KH chấp nhận, NBC sẽ kết nối bạn với KH đó. Mỗi lái xe chỉ được báo giá 1 lần.',
                scope: $scope,
                buttons: [{
                    text: 'Hủy'
                }, {
                    text: '<b>Đồng Ý</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        return $scope.bid.priceinf;
                    }
                }, ]
            });
            pricePopup.then(function(res) {
                if (res) {
                    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=priceinf&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&price=" + res + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                    $http.jsonp(url).
                    success(function(data, status, headers, config) {
                        if (data.success == 1) {
                            route.priceinf = res*1000;
                            $ionicPopup.alert({
                                title: 'Thông báo',
                                template: 'Giá bạn yêu cầu đã được gửi đến tổng đài.'
                            });
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Lỗi',
                                template: data.detail
                            });
                        }
                    }).
                    error(function(data, status, headers, config) {
                        $ionicPopup.alert({
                            title: 'Lỗi',
                            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                        });
                    });
                }
            });
        };

        $ionicModal.fromTemplateUrl('templates/filter.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openFilter = function() {
            $scope.modal.show();
        }

        $scope.closeFilter = function() {
            $scope.modal.hide();
        }
    })

.controller('WinListCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $filter, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {

    $scope.listCanSwipe = true;
    var tomorrow = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
    var next7days = new Date(new Date().getTime() + 24 * 60 * 60 * 1000 * 7);

    $scope.filter = {"from_date": tomorrow, "to_date" : next7days};

    from_date = $scope.filter.from_date.toISOString().substring(0, 10);
    to_date = $scope.filter.to_date.toISOString().substring(0, 10);

    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getWonList&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

    $http.jsonp(url).
    success(function(data, status, headers, config) {
        if (data.success == 1) {
            $scope.routes = data.data;
            $rootScope.total_price = data.total;
            $rootScope.total_cut = data.diff;
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                template: data.detail
            });
        }
    }).
    error(function(data, status, headers, config) {
        $ionicPopup.alert({
            title: 'Lỗi',
            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
        });
    });

    $scope.doRefresh = function() {
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getWonList&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.routes = data.data;
                $rootScope.total_price = data.total;
                $rootScope.total_cut = data.diff;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
                $ionicPopup.alert({
                    title: 'Lỗi',
                    template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                });
            })
            .finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
    }

    $scope.message = function(route) {
            $scope.confirm = {message: route.driver_message};
            var buyPopup = $ionicPopup.show({
                template: '<input type="text" ng-model="confirm.message">',
                title: 'Nhập thông tin xác nhận',
                scope: $scope,
                buttons: [{
                    text: 'Hủy',
                    onTap: function(e){
                        return false;
                    }
                }, {
                    text: '<b>Đồng Ý</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        if($scope.confirm.message != undefined && $scope.confirm.message.trim() != '')
                            route.driver_message = $scope.confirm.message;
                        return $scope.confirm.message;
                    }
                }, ]
            });
            buyPopup.then(function(res) {
                if(res == false)
                    return;
                if(res == undefined || res.trim() == ''){
                    var alertPopup = $ionicPopup.alert({
                        title: 'Lỗi',
                        template: 'Phải nhập thông tin trước khi xác nhận!'
                    });
                    return;
                }
                if (res) {
                    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=confirm&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&value=" + res + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                    $http.jsonp(url).
                    success(function(data, status, headers, config) {
                        if (data.success == 1) {
                            $ionicPopup.alert({
                                title: 'Thông báo',
                                template: 'Xác nhận thành công!'
                            });
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Lỗi',
                                template: data.detail
                            });
                        }
                    }).
                    error(function(data, status, headers, config) {
                        $ionicPopup.alert({
                            title: 'Lỗi',
                            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                        });
                    });
                }
            });
        };

    $scope.locdau = function(str){

        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");

        str= str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g,"A");
        str= str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g,"E");
        str= str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g,"I");
        str= str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g,"O");
        str= str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g,"U");
        str= str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g,"Y");
        str= str.replace(/Đ/g,"D");

        return str
    }

    $scope.copyMessage = function(route){

        var tempstr = '<textarea rows="4">Đón '+route.customer_name + ' ('+ route.customer_phone + ') lúc ' + $filter('date')($scope.formatDate(route.pickup_datetime), 'dd/MM - HH:mm a') + ' từ ' + route.start + ' đến ' + route.destination + (route.is_debt==1? '.': '. Thu khách '+ $scope.formatCurrency(route.customer_price)) +'</textarea>';
        tempstr = $scope.locdau(tempstr);
        var buyPopup = $ionicPopup.show({
            template: tempstr,
            title: 'Thông tin SMS',
            scope: $scope,
            buttons: [{
                text: 'Hủy'
            }, {
                text: 'Gửi SMS',
                type: 'button-light'
            }, ]
        });
    }

    $scope.filterDate = function() {
        from_date = $scope.filter.from_date.toISOString().substring(0, 10);
        to_date = $scope.filter.to_date.toISOString().substring(0, 10);

        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getWonList&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.routes = data.data;
                $rootScope.total_price = data.total;
                $rootScope.total_cut = data.diff;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        })
    };


    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

    $scope.formatCurrency = function(price) {
        return (price / 1000).toLocaleString() + 'k';
    };

})

.controller('TopupListCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $filter, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {

    var today = new Date();
    var prev7days = new Date(new Date().getTime() - 24 * 60 * 60 * 1000 * 7);

    $scope.filter = {"from_date": prev7days, "to_date" : today};

    from_date = $scope.filter.from_date.toISOString().substring(0, 10);
    to_date = $scope.filter.to_date.toISOString().substring(0, 10);

    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getTopup&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

    $http.jsonp(url).
    success(function(data, status, headers, config) {
        if (data.success == 1) {
            $scope.topups = data.data;
            $rootScope.total_price = data.total;
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                template: data.detail
            });
        }
    }).
    error(function(data, status, headers, config) {
        $ionicPopup.alert({
            title: 'Lỗi',
            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
        });
    });

    $scope.doRefresh = function() {
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getTopup&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.topups = data.data;
                $rootScope.total_price = data.total;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
                $ionicPopup.alert({
                    title: 'Lỗi',
                    template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                });
            })
            .finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
    }

    $scope.filterDate = function() {
        from_date = $scope.filter.from_date.toISOString().substring(0, 10);
        to_date = $scope.filter.to_date.toISOString().substring(0, 10);

        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getTopup&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.topups = data.data;
                $rootScope.total_price = data.total;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        })
    };


    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

    $scope.formatCurrency = function(price) {
        return (price / 1000).toLocaleString() + 'k';
    };

})

.controller('MessageListCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {

    // $scope.listCanSwipe = true;

    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getMessages&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

    $http.jsonp(url).
    success(function(data, status, headers, config) {
        if (data.success == 1) {
            $rootScope.messages = data.data;
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                template: data.detail
            });
        }
    }).
    error(function(data, status, headers, config) {
        $ionicPopup.alert({
            title: 'Lỗi',
            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
        });
    });

    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

    $scope.doRefresh = function() {
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getMessages&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $rootScope.messages = data.data;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        })
        .finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

})

.controller('JoinListCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {

    $scope.listCanSwipe = true;

    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getJoinList&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

    $http.jsonp(url).
    success(function(data, status, headers, config) {
        if (data.success == 1) {
            $scope.routes = data.data;
            $rootScope.total_price = data.total;
            $rootScope.total_cut = data.diff;
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                template: data.detail
            });
        }
    }).
    error(function(data, status, headers, config) {
        $ionicPopup.alert({
            title: 'Lỗi',
            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
        });
    });

    $scope.doRefresh = function() {
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getJoinList&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.routes = data.data;
                $rootScope.total_price = data.total;
                $rootScope.total_cut = data.diff;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
                $ionicPopup.alert({
                    title: 'Lỗi',
                    template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                });
            })
            .finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
    }

    $scope.message = function(route) {
            $scope.confirm = {};
            var buyPopup = $ionicPopup.show({
                template: '<input type="text" ng-model="confirm.message">',
                title: 'Nhập thông tin xác nhận',
                scope: $scope,
                buttons: [{
                    text: 'Hủy',
                    onTap: function(e){
                        return false;
                    }
                }, {
                    text: '<b>Đồng Ý</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        if($scope.confirm.message != undefined && $scope.confirm.message.trim() != '')
                            route.driver_message = $scope.confirm.message;
                        return $scope.confirm.message;
                    }
                }, ]
            });
            buyPopup.then(function(res) {
                if(res == false)
                    return;
                if(res == undefined || res.trim() == ''){
                    var alertPopup = $ionicPopup.alert({
                        title: 'Lỗi',
                        template: 'Phải nhập thông tin trước khi xác nhận!'
                    });
                    return;
                }
                if (res) {
                    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=confirm&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&value=" + res + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                    $http.jsonp(url).
                    success(function(data, status, headers, config) {
                        if (data.success == 1) {
                            $ionicPopup.alert({
                                title: 'Thông báo',
                                template: 'Xác nhận thành công!'
                            });
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Lỗi',
                                template: data.detail
                            });
                        }
                    }).
                    error(function(data, status, headers, config) {
                        $ionicPopup.alert({
                            title: 'Lỗi',
                            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                        });
                    });
                }
            });
    };

    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

    $scope.formatCurrency = function(price) {
        return (price / 1000).toLocaleString() + 'k';
    };

})

.controller('MessageDetailCtrl', function($scope, $stateParams, $rootScope, tokenStore, $http, $ionicPopup) {
    $scope.message = $rootScope.messages.filter(function(entry) {
        $scope.m_content = entry.m_content;
        return entry.m_id == $stateParams.messageId;
    })[0];

    if($scope.message.is_read == 0){
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=markAsReadMessage&username=" + tokenStore.getUsenamer() + "&id=" + $scope.message.m_id + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.message.is_read = 1;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        });
    }

    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

})
.controller('SettingCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $filter, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {
    $scope.password = {'old': '', 'new': '', 'renew': ''};
    $scope.changePassword = function() {

        var old_pass = $scope.password.old,
            new_pass = $scope.password.new,
            re_new = $scope.password.renew;
            
        if(new_pass.trim().length < 5){
            $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Mật khẩu mới phải nhiều hơn 5 kí tự!'
            });
            return;
        }

        if(new_pass == old_pass){
            $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Mật khẩu mới và mật khẩu cũ không được trùng nhau!'
            });
            return;
        }

        if(new_pass != re_new){
            $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Vui lòng nhập lại Mật khẩu mới khớp với Mật khẩu bạn muốn tạo!'
            });
            return;
        }

        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=changePassword&username=" + tokenStore.getUsenamer() + "&old_pass=" + old_pass + "&new_pass=" + new_pass + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                var confirmPopup = $ionicPopup.alert({
                    title: 'Đổi mật khẩu thành công',
                    template: 'Bạn phải <b>Thoát</b> khỏi hệ thống và đăng nhập lại để thay đổi có hiệu lực.'
                });
                confirmPopup.then(function(res) {
                    if (res) {
                        $scope.password = {'old': '', 'new': '', 'renew': ''};
                        localStorage.removeItem('token');
                        localStorage.removeItem('username');
                        $location.path('/login');
                        $scope.$apply();
                        $window.location.reload();
                    }
                });
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        });
    }


})
.controller('DetailCtrl', function($scope, $stateParams, BidListService, $ionicPopup, tokenStore, $http, $state) {

    BidListService.find($stateParams.routeId, function(route) {
        $scope.route = route;
        if (route.biduserlist != "")
            $scope.biduserlist = angular.fromJson(route.biduserlist);
    });

    $scope.returnMilliseconds = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut.getTime();
    };

    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

    $scope.formatCurrency = function(price) {
        return (price / 1000).toLocaleString() + 'k';
    };

    $scope.buy = function(route) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Xác nhận',
            template: 'Bạn có chắc muốn mua ngay lịch này với giá <b>' + $scope.formatCurrency(route.buy_price) + '</b>?'
        });
        confirmPopup.then(function(res) {
            if (res) {
                //Nếu đồng ý Mua
                var url = "https://www.noibaiconnect.com/bidcore/query.php?action=buy&price="+route.buy_price+"&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                $http.jsonp(url).
                success(function(data, status, headers, config) {
                    if (data.success == 1) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Thông báo',
                            template: 'Bạn đã mua <b>thành công</b>!'
                        });
                        alertPopup.then(function() {
                            $state.go('app.bidlist');

                        });
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Lỗi',
                            template: data.detail
                        });
                    }
                }).
                error(function(data, status, headers, config) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Lỗi',
                        template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                    });
                });
            }
        });

    };

    $scope.bid = function(route) {
        var buyPopup = $ionicPopup.show({
            template: '<input type="number" ng-model="bid.price">',
            title: 'Nhập giá mà bạn muốn đi',
            subTitle: 'Ví dụ: Nếu muốn đi với giá 350.000đ xin nhập 350 vào ô dưới.<br> Giá hiện tại đang là <b>'+$scope.formatCurrency(route.current_price)+'</b>',
            scope: $scope,
            buttons: [{
                text: 'Hủy'
            }, {
                text: '<b>Đồng Ý</b>',
                type: 'button-positive',
                onTap: function(e) {
                    // if (!$scope.bid.price) {
                    //     //don't allow the user to close unless he enters bid price
                    //     e.preventDefault();
                    // } else {
                        return $scope.bid.price;
                    // }
                }
            }, ]
        });
        buyPopup.then(function(res) {
            if (res) {
                if (route.current_price - (res*1000) < 10000 || (route.current_price - (res*1000))%10000 != 0) {
                    $ionicPopup.alert({
                        title: 'Lỗi',
                        template: 'Số tiền trả giá phải nhỏ hơn <b>'+$scope.formatCurrency(route.current_price)+'</b> và chẵn <b>10k</b>', 
                    });
                    return;
                }
                // if (res >= route.current_price + 10000) {
                //     $ionicPopup.alert({
                //         title: 'Lỗi',
                //         template: 'Bước trả giá phải lớn hơn 10k!'
                //     });
                //     return;
                // }
                //Nếu đồng ý Mua
                var url = "https://www.noibaiconnect.com/bidcore/query.php?action=bid&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&price=" + res + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                $http.jsonp(url).
                success(function(data, status, headers, config) {
                    if (data.success == 1) {
                        $ionicPopup.alert({
                            title: 'Thông báo',
                            template: 'Bạn đã trả giá <b>hợp lệ</b>!'
                        });
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Lỗi',
                            template: data.detail
                        });
                    }
                }).
                error(function(data, status, headers, config) {
                    $ionicPopup.alert({
                        title: 'Lỗi',
                        template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                    });
                });
            }
        });
    };

    $scope.priceinf = function(route) {
        var pricePopup = $ionicPopup.show({
            template: '<input type="number" ng-model="bid.priceinf">',
            title: 'Nhập giá mà bạn muốn nhận cuốc này',
            subTitle: 'Giá bạn đưa ra sẽ được dùng để thương lượng với KH. Nếu KH chấp nhận, NBC sẽ kết nối bạn với KH đó. Mỗi lái xe chỉ được báo giá 1 lần.',
            scope: $scope,
            buttons: [{
                text: 'Hủy'
            }, {
                text: '<b>Đồng Ý</b>',
                type: 'button-positive',
                onTap: function(e) {
                    return $scope.bid.priceinf;
                }
            }, ]
        });
        pricePopup.then(function(res) {
            if (res) {
                var url = "https://www.noibaiconnect.com/bidcore/query.php?action=priceinf&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&price=" + res + "&tid=" + route.id + "&callback=JSON_CALLBACK";

                $http.jsonp(url).
                success(function(data, status, headers, config) {
                    if (data.success == 1) {
                        route.priceinf = res*1000;
                        $ionicPopup.alert({
                            title: 'Thông báo',
                            template: 'Giá bạn yêu cầu đã được gửi đến tổng đài.'
                        });
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Lỗi',
                            template: data.detail
                        });
                    }
                }).
                error(function(data, status, headers, config) {
                    $ionicPopup.alert({
                        title: 'Lỗi',
                        template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                    });
                });
            }
        });
    };

})

.directive('backImg', function(){
    return function(scope, element, attrs){
        var url = attrs.backImg;
        var content = element.find('a');
        content.css({
            'background': 'url(' + url +')'            
        });
    };
});
