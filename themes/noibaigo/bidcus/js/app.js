// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'emguo.poller', 'ngResource'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })
        .state('app.pendinglist', {
            url: '/pendinglist',
            views: {
                'menuContent': {
                    templateUrl: 'templates/pendinglist.html',
                    controller: 'PendinglistsCtrl'
                }
            }
        })
        .state('app.login', {
            url: '/login',
            views: {
                'menuContent': {
                    templateUrl: 'templates/login.html',
                    controller: 'LoginCtrl'
                }
            }
        })

    .state('app.soldlist', {
        url: '/soldlist',
        views: {
            'menuContent': {
                templateUrl: 'templates/soldlist.html',
                controller: 'SoldListCtrl'
            }
        }
    })

    .state('app.pricelist', {
        url: '/pricelist',
        views: {
            'menuContent': {
                templateUrl: 'templates/pricelist.html',
                controller: 'PriceListCtrl'
            }
        }
    })

    .state('app.message', {
        url: '/message',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/message.html',
                controller: 'MessageListCtrl'
            }
        }
    })

    .state('app.singlemessage', {
        url: '/messagelist/:messageId',
        views: {
            'menuContent': {
                templateUrl: 'templates/messagedetail.html',
                controller: 'MessageDetailCtrl'
            }
        }
    })

    .state('app.help', {
        url: '/help',
        views: {
            'menuContent': {
                templateUrl: 'templates/help.html'
            }
        }
    });


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
