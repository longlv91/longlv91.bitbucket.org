angular.module('starter.controllers', [])

.factory('tokenStore', function() {
    var token = username = '';
    return {
        getToken: function() {
            return token;
        },
        getUsenamer: function() {
            return username;
        },
        setToken: function(newToken) {
            token = newToken;
        },
        setUsername: function(newUsername) {
            username = newUsername;
        }
    }
})

.factory('getPendingList', function($resource, tokenStore) {
    return {
        getResource: function() {
            return $resource('https://www.noibaiconnect.com/bidcore/query.php', {
                action: 'getPendingList',
                username: tokenStore.getUsenamer(),
                token: tokenStore.getToken(),
                callback: 'JSON_CALLBACK'

            }, {
                jsonp_get: {
                    method: 'JSONP'
                }
            });
        }
    }
})

.factory('PendingListService', function($q, poller, getPendingList) {
    var routes = [];
    // var myPoller;
    return {
        getData: function(callback) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            myPoller = poller.get(getPendingList.getResource(), {
                action: 'jsonp_get',
                delay: 30000
            });
            myPoller.promise.then(null, null, function(response) {
                if (response.success == 1) {
                    routes = (response);
                    callback(routes);
                } else {
                    deferred.reject(response.detail);
                }
            });

            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        },
        stopPoller: function() {
            myPoller.stop();
        },
        find: function(routeId, callback) {
            var route = routes.data.filter(function(entry) {
                return entry.id == routeId;
            })[0];
            callback(route);
        }
    }
})

.service('LoginService', function($q, $http) {
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            var url = "https://www.noibaiconnect.com/bidcore/query.php?action=loginB&username=0" + name + "&password=" + pw + "&callback=JSON_CALLBACK";

            $http.jsonp(url).
            success(function(data, status, headers, config) {
                if (data.success == 1) {
                    deferred.resolve(data);
                } else
                    deferred.reject(data.detail);
            }).
            error(function(data, status, headers, config) {
                deferred.reject('Thiết bị của bạn đang không được kết nối internet hoặc 3G!');
            });

            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})

.controller('AppCtrl', function($scope, $ionicPopup, $window, $location, $rootScope, $interval) {
    $rootScope.total_price = 0;
    $rootScope.total_cut = 0;

    $scope.Logout = function() {

        var confirmPopup = $ionicPopup.confirm({
            title: 'Thoát',
            okType: 'button-assertive',
            template: 'Bạn có chắc muốn thoát khỏi chương trình?'
        });
        confirmPopup.then(function(res) {
            if (res) {
                $location.path('/login');
                $scope.$apply();
                $window.location.reload();
            }
        });
    }

    $scope.timerRunning = true;

})

.controller('LoginCtrl', function($scope, $ionicSideMenuDelegate, $state, LoginService, tokenStore, $ionicPopup, $ionicLoading) {
    // Form data for the login modal
    $scope.loginData = {};
    $ionicSideMenuDelegate.canDragContent(false)
        // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        LoginService.loginUser($scope.loginData.username, $scope.loginData.password).success(function(data) {
            $ionicLoading.hide();
            tokenStore.setToken(data.token);
            tokenStore.setUsername(data.username);
            $state.go('app.pendinglist');
            $scope.loginData = {};
        }).error(function(data) {
            $ionicLoading.hide();
            $scope.loginData.password = '';
            var alertPopup = $ionicPopup.alert({
                title: 'Đăng nhập thất bại',
                template: data
            });
        });
    };

})

.controller('PendinglistsCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $ionicPopup, PendingListService, $location, $window, $ionicLoading, tokenStore, $http, $state) {

        $scope.routes = new Array();
        $scope.register = {"phone": '', "name" : "", "vat": false, "is_round_trip": false, "from_airport": true, "to_airport": false, "pickup_datetime": "", "desc": "", "seats": 4};
        if ($scope.routes.length <= 0) {
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
        }

        PendingListService.getData(function(data) {
            $rootScope.messageCount = data.message_count;
            $scope.message_title = data.message_title;

            var data = data.data;
            $ionicLoading.hide();
            $scope.routes = data;
        }).error(function(data) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: data
            });
            alertPopup.then(function(res) {
                $location.path('/login');
                $scope.$apply();
                $window.location.reload();
            });
        });

        $scope.viewMessage = function(){
            $state.go('app.message');
        }

        $scope.clearSearch = function() {
            $scope.searchFilter = '';
        };


        $scope.formatDate = function(date) {
            var userAgent = navigator.userAgent;
            var browser = 'chrome';
            var browsers = {
                chrome: /chrome/i,
                safari: /safari/i,
                firefox: /firefox/i,
                ie: /internet explorer/i
            };

            for (var key in browsers) {
                if (browsers[key].test(userAgent)) {
                    browser = key;
                }
            }

            if (ionic.Platform.isIOS() || browser == 'safari') {
                date = date.replace(' ', 'T');
            }

            if (ionic.Platform.isAndroid() || browser == 'chrome') {
                date = date.replace(' ', 'T');
                date += 'Z';
            }

            var dateOut = new Date(date);
            dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
            return dateOut;
        };


        $scope.formatCurrency = function(price) {
            return (price / 1000).toLocaleString() + 'k';
        };

        $ionicModal.fromTemplateUrl('templates/form.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.sendRoute = function() {
            if($scope.register.phone == null || $scope.register.phone == 0){
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    okType: 'button-assertive',
                    template: 'Xin vui lòng nhập số điện thoại khách hàng!'
                });
                return;
            }
            var date = new Date($scope.register.pickup_datetime);
            var p_datetime = date.getFullYear() + '-' +(date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
            var url = "https://www.noibaiconnect.com/bidcore/query.php?action=sendRoute&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&phone=0" + $scope.register.phone + "&name=" + $scope.register.name + "&vat=" + $scope.register.vat + "&is_round_trip=" + $scope.register.is_round_trip + "&start_place=" + $scope.register.start_place + "&from_airport=" + $scope.register.from_airport + "&destination_place=" + $scope.register.destination_place + "&to_airport=" + $scope.register.to_airport + "&pickup_datetime=" + p_datetime + "&seats=" + $scope.register.seats + "&desc=" + $scope.register.desc + "&callback=JSON_CALLBACK";

            $http.jsonp(url).
            success(function(data, status, headers, config) {
                if (data.success == 1) {
                    $scope.routes.push({customer_phone: '0'+$scope.register.phone, status_id: 1, customer_name: '', start: '', pickup_datetime: p_datetime});
                    $ionicPopup.alert({
                        title: 'Thông báo',
                        okType: 'button-assertive',
                        template: 'Thông tin khách hàng đã được gửi lên hệ thống của NBC.'
                    });
                    $scope.register = {"phone": '', "name" : "", "vat": false, "is_round_trip": false, "from_airport": true, "to_airport": false, "pickup_datetime": "", "desc": "", "seats": 4};
                    $scope.modal.hide();
                } else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Lỗi',
                        okType: 'button-assertive',
                        template: data.detail
                    });
                }
            }).
            error(function(data, status, headers, config) {
                $ionicPopup.alert({
                    title: 'Lỗi',
                    template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                });
            });
        };

        $scope.callMe = function(){
            var url = "https://www.noibaiconnect.com/bidcore/query.php?action=sendRoute&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&phone=-1&callback=JSON_CALLBACK";

            $http.jsonp(url).
            success(function(data, status, headers, config) {
                if (data.success == 1) {
                    $scope.routes.push({customer_phone: '0', status_id: 1, customer_name: '', start: ''});
                    $ionicPopup.alert({
                        title: 'Thông báo',
                        okType: 'button-assertive',
                        template: 'Tổng đài sẽ gọi lại cho bạn sau ít phút nữa!'
                    });
                    $scope.register = {"phone": '', "desc" : ""};
                    $scope.modal.hide();
                } else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Lỗi',
                        okType: 'button-assertive',
                        template: data.detail
                    });
                }
            }).
            error(function(data, status, headers, config) {
                $ionicPopup.alert({
                    title: 'Lỗi',
                    template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                });
            });
        }

        $scope.openQuickForm = function() {
            var myPopup = $ionicPopup.show({
                template: 'Số Điện Thoại<input type="number" ng-model="register.phone">   <br> Ghi Chú  <textarea rows="5" ng-model="register.desc"></textarea>',
                title: 'Đăng ký nhanh',
                scope: $scope,
                buttons: [{
                    text: 'Hủy'
                }, {
                    text: '<b>Gửi</b>',
                    type: 'button-assertive',
                    onTap: function(e) {
                        if (!$scope.register.phone) {
                            //don't allow the user to close unless he enters wifi password
                            e.preventDefault();
                        } else {
                            return $scope.register;
                        }
                    }
                }]
            });

            myPopup.then(function(res) {
                if (res) {
                    if(res.phone == null || res.phone == 0){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Lỗi',
                            okType: 'button-assertive',
                            template: 'Xin vui lòng nhập số điện thoại khách hàng!'
                        });
                        return;
                    }
                    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=sendRoute&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&phone=0" + res.phone + "&desc=" + res.desc + "&callback=JSON_CALLBACK";

                    $http.jsonp(url).
                    success(function(data, status, headers, config) {
                        if (data.success == 1) {
                            $scope.routes.push({customer_phone: '0'+res.phone, status_id: 1, customer_name: '', start: ''});
                            $ionicPopup.alert({
                                title: 'Thông báo',
                                okType: 'button-assertive',
                                template: 'Thông tin khách hàng đã được gửi lên hệ thống của NBC.'
                            });
                            $scope.register = {"phone": '', "desc" : ""};
                            $scope.modal.hide();
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Lỗi',
                                okType: 'button-assertive',
                                template: data.detail
                            });
                        }
                    }).
                    error(function(data, status, headers, config) {
                        $ionicPopup.alert({
                            title: 'Lỗi',
                            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                        });
                    });
                } else {
                    console.log('Enter password');
                }
            });
        }

        $scope.openForm = function() {
            $scope.modal.show();
        }

        $scope.closeForm = function() {
            $scope.modal.hide();
        }

        $scope.doRefresh = function() {
            var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getPendingList&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

            $http.jsonp(url).
            success(function(data, status, headers, config) {
                if (data.success == 1) {
                    $scope.routes = data.data;
                } else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Lỗi',
                        okType: 'button-assertive',
                        template: data.detail
                    });
                }
            }).
            error(function(data, status, headers, config) {
                $ionicPopup.alert({
                    title: 'Lỗi',
                    okType: 'button-assertive',
                    template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
                });
            })
            .finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
        }

        $scope.togglePlace = function(val){
            if(val == 1){
                if($scope.register.from_airport == true)
                    $scope.register.to_airport = false;
            }
            
            if(val == 2){
                if($scope.register.to_airport == true)
                    $scope.register.from_airport = false;
            }
        }
    })

.controller('PriceListCtrl', function($scope, $ionicModal, $ionicListDelegate, $ionicScrollDelegate, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {

    $scope.places = new Array();
    $scope.search = {'query': ''};
    $scope.queryPlace = function() {
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getPriceList&username=" + tokenStore.getUsenamer() + "&query=" + $scope.search.query + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.places = data.data;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    okType: 'button-assertive',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        })
    };      

    $scope.clearSearch = function() {
        $scope.search.query = '';
    };

    $scope.formatCurrency = function(price) {
        return (price / 1000).toLocaleString() + 'k';
    };

})

.controller('MessageListCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {

    // $scope.listCanSwipe = true;

    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getMessages&type=2&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

    $http.jsonp(url).
    success(function(data, status, headers, config) {
        if (data.success == 1) {
            $rootScope.messages = data.data;
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: data.detail
            });
        }
    }).
    error(function(data, status, headers, config) {
        $ionicPopup.alert({
            title: 'Lỗi',
            okType: 'button-assertive',
            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
        });
    });

    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

    $scope.doRefresh = function() {
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getMessages&type=2&username=" + tokenStore.getUsenamer() + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $rootScope.messages = data.data;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    okType: 'button-assertive',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        })
        .finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

})

.controller('MessageDetailCtrl', function($scope, $stateParams, $rootScope, tokenStore, $http, $ionicPopup) {
    $scope.message = $rootScope.messages.filter(function(entry) {
        $scope.m_content = entry.m_content;
        return entry.m_id == $stateParams.messageId;
    })[0];

    if($scope.message.is_read == 0){
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=markAsReadMessage&username=" + tokenStore.getUsenamer() + "&id=" + $scope.message.m_id + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.message.is_read = 1;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    okType: 'button-assertive',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        });
    }

    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

})

.controller('SoldListCtrl', function($scope, $rootScope, $ionicModal, $timeout, $ionicListDelegate, $ionicScrollDelegate, $ionicPopup, $location, $window, $ionicLoading, tokenStore, $http) {

    var tomorrow = new Date(new Date().getFullYear(), new Date().getMonth(), 1);//the first day of month
    // var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var next7days = new Date();
    // var next7days = new Date(new Date().getTime() + 24 * 60 * 60 * 1000 * 7);
    // tomorrow = tomorrow.toISOString().substring(0, 10);
    // next7days = next7days.toISOString().substring(0, 10);
    $scope.filter = {"from_date": tomorrow, "to_date" : next7days};

    from_date = $scope.filter.from_date.toISOString().substring(0, 10);
    to_date = $scope.filter.to_date.toISOString().substring(0, 10);

    var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getSoldList&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

    $http.jsonp(url).
    success(function(data, status, headers, config) {
        if (data.success == 1) {
            $scope.routes = data.data;
            $rootScope.total_price = data.total;
            $rootScope.total_cut = data.diff;
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: data.detail
            });
        }
    }).
    error(function(data, status, headers, config) {
        $ionicPopup.alert({
            title: 'Lỗi',
            okType: 'button-assertive',
            template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
        });
    });

    $scope.doRefresh = function() {
        from_date = $scope.filter.from_date.toISOString().substring(0, 10);
        to_date = $scope.filter.to_date.toISOString().substring(0, 10);
        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getSoldList&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.routes = data.data;
                $rootScope.total_price = data.total;
                $rootScope.total_cut = data.diff;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    okType: 'button-assertive',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        })
        .finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    }


    $scope.filterDate = function() {
        from_date = $scope.filter.from_date.toISOString().substring(0, 10);
        to_date = $scope.filter.to_date.toISOString().substring(0, 10);

        var url = "https://www.noibaiconnect.com/bidcore/query.php?action=getSoldList&username=" + tokenStore.getUsenamer() + "&from_date=" + from_date + "&to_date=" + to_date + "&token=" + tokenStore.getToken() + "&callback=JSON_CALLBACK";

        $http.jsonp(url).
        success(function(data, status, headers, config) {
            if (data.success == 1) {
                $scope.routes = data.data;
                $rootScope.total_price = data.total;
                $rootScope.total_cut = data.diff;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Lỗi',
                    okType: 'button-assertive',
                    template: data.detail
                });
            }
        }).
        error(function(data, status, headers, config) {
            $ionicPopup.alert({
                title: 'Lỗi',
                okType: 'button-assertive',
                template: 'Thiết bị của bạn đang không được kết nối internet hoặc 3G!'
            });
        })
    };

    $scope.clearSearch = function() {
        $scope.searchFilter = '';
    };


    $scope.formatDate = function(date) {
        var userAgent = navigator.userAgent;
        var browser = 'chrome';
        var browsers = {
            chrome: /chrome/i,
            safari: /safari/i,
            firefox: /firefox/i,
            ie: /internet explorer/i
        };

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                browser = key;
            }
        }

        if (ionic.Platform.isIOS() || browser == 'safari') {
            date = date.replace(' ', 'T');
        }

        if (ionic.Platform.isAndroid() || browser == 'chrome') {
            date = date.replace(' ', 'T');
            date += 'Z';
        }

        var dateOut = new Date(date);
        dateOut.setTime(dateOut.getTime() + dateOut.getTimezoneOffset() * 60 * 1000);
        return dateOut;
    };

    $scope.formatCurrency = function(price) {
        return (price / 1000).toLocaleString() + 'k';
    };

});
