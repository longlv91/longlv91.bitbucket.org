jQuery(function($) {
	'use strict',
	// all Parallax Section
	$(window).load(function(){'use strict',
		$("#services").parallax("50%", 0.3);
		$("#clients").parallax("50%", 0.3);
	});
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
    function ellipsisText(text) {
        if (text && text.length > 18) {
            return text.substring(0, 15) + "...";
        } else {
            return text;
        }
    }
	// portfolio filter
	$(window).load(function(){
        var listTemplates = [
            {
                name: "Blue App Template",
                url: "themes/1",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/1.jpg",
                description: ""
            },
            {
                name: "BLUE ONEPAGE HTML5 TEMPLATE",
                url: "themes/2",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/2.jpg",
                description: ""
            },
            {
                name: "Cluster - Creative Portfolio HTML Template",
                url: "themes/3",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/3.jpg",
                description: ""
            },
            {
                name: "DOCTOR - Responsive HTML & Bootstrap Template",
                url: "themes/4",
                category: "Medicine",
                image_url: "images/portfolio/4.jpg",
                description: ""
            },
            {
                name: "eElectronics - HTML eCommerce Template",
                url: "themes/5",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/5.jpg",
                description: ""
            },
            {
                name: "Egret - HTML5 landing page",
                url: "themes/6",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/6.jpg",
                description: ""
            },
            {
                name: "Home | E-Shopper",
                url: "themes/7",
                category: "Business",
                image_url: "images/portfolio/7.jpg",
                description: ""
            },
            {
                name: "Evento | Free Onepage Event Template | ShapeBootstrap",
                url: "themes/8",
                category: "Music",
                image_url: "images/portfolio/8.jpg",
                description: ""
            },
            {
                name: "FIMPLY | One page HTML template",
                url: "themes/9",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/9.jpg",
                description: ""
            },
            {
                name: "Heera HTML5 Template by Jewel Theme",
                url: "themes/10",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/10.jpg",
                description: ""
            },
            {
                name: "HIMU - OnePage HTML Parallax template",
                url: "themes/11",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/11.jpg",
                description: ""
            },
            {
                name: "Leroy - Onepage Bootstrap HTML Template",
                url: "themes/12",
                category: "Food",
                image_url: "images/portfolio/12.jpg",
                description: ""
            },
            {
                name: "Ino Soon- Responsive Coming Soon Template",
                url: "themes/13",
                category: "Coming soon",
                image_url: "images/portfolio/13.jpg",
                description: ""
            },
            {
                name: "Bootstrap Metro Dashboard by Dennis Ji for ARM demo",
                url: "themes/14",
                category: "Administrator",
                image_url: "images/portfolio/14.jpg",
                description: ""
            },
            {
                name: "Jonaki | Job Board Template",
                url: "themes/15",
                category: "Searching",
                image_url: "images/portfolio/15.jpg",
                description: ""
            },
            {
                name: "LUCID | HTML5 Responsive App Landing Page",
                url: "themes/16",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/16.jpg",
                description: ""
            },
            {
                name: "Maxima | Responsive Multipurpose Bootstrap Theme",
                url: "themes/17",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/17.jpg",
                description: ""
            },
            {
                name: "Responsive Onepage HTML Template | Multi",
                url: "themes/18",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/18.jpg",
                description: ""
            },
            {
                name: "Sept - StratUp Landing Page",
                url: "themes/19",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/19.jpg",
                description: ""
            },
            {
                name: "Smallapps HTML Template",
                url: "themes/20",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/20.jpg",
                description: ""
            },
            {
                name: "Agency - Start Bootstrap Theme",
                url: "themes/21",
                category: "Fashion",
                image_url: "images/portfolio/21.jpg",
                description: ""
            },
            {
                name: "Creative - Start Bootstrap Theme",
                url: "themes/22",
                category: "Bootstrap Responsive",
                image_url: "",
                description: ""
            },
            {
                name: "Landing Page - Start Bootstrap Theme",
                url: "themes/23",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/23.jpg",
                description: ""
            },
            {
                name: "SB Admin 2 - Bootstrap Admin Theme",
                url: "themes/24",
                category: "Administrator",
                image_url: "images/portfolio/24.jpg",
                description: ""
            },
            {
                name: "SB Admin - Bootstrap Admin Template",
                url: "themes/25",
                category: "Administrator",
                image_url: "images/portfolio/25.jpg",
                description: ""
            },
            {
                name: "Spirit8 - Digital Agency One Page Template",
                url: "themes/26",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/26.jpg",
                description: ""
            },
            {
                name: "Timer Agency Template",
                url: "themes/27",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/27.jpg",
                description: ""
            },
            {
                name: "Home | Triangle",
                url: "themes/28",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/28.jpg",
                description: ""
            },
            {
                name: "Unika - Responsive One Page HTML5 Template",
                url: "themes/29",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/29.jpg",
                description: ""
            },
            {
                name: "ubutia | Home",
                url: "themes/30",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/30.jpg",
                description: ""
            },
            {
                name: "New Age - Start Bootstrap Theme",
                url: "themes/31",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/31.jpg",
                description: ""
            },
            {
                name: "Cyrus Studio Template",
                url: "themes/32",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/32.jpg",
                description: ""
            },
            {
                name: "Platz - Free Grid Wordpress Theme",
                url: "themes/33",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/33.jpg",
                description: ""
            },
            {
                name: "Magnetic Theme",
                url: "themes/34",
                category: "Bootstrap Responsive",
                image_url: "images/portfolio/34.JPG",
                description: ""
            }
        ], length = listTemplates.length, i, listCategories = [];
        var parentFilter = $('.portfolio-filter'),
            parentItem = $('.portfolio-items'),
            portfolioFilter = $('.portfolio-filter > .template-ele'),
            portfolioItem = $('.portfolio-items > .template-ele'), cloneFilter, cloneItem;
        for (i = 0; i < length; i++) {
            cloneFilter = portfolioFilter.clone();
            cloneItem = portfolioItem.clone();
            $(cloneFilter).removeClass("template-ele");
            $(cloneItem).removeClass("template-ele");
            if (listCategories.indexOf(listTemplates[i].category) == -1) {
                listCategories.push(listTemplates[i].category);
                $(cloneFilter).find("a")[0].setAttribute("data-filter", "." + listTemplates[i].category.trim().toLowerCase().replaceAll(" ", "_"));
                $($(cloneFilter).find("a")[0]).html(listTemplates[i].category);
                parentFilter.append(cloneFilter);
            }
            $(cloneItem).addClass(listTemplates[i].category.trim().toLowerCase().replaceAll(" ", "_"));
            $(cloneItem).find(".portfolio-image img")[0].setAttribute("src", listTemplates[i].image_url);
            $(cloneItem).find(".mask h3").html(ellipsisText(listTemplates[i].name));
            $(cloneItem).find(".mask a")[0].setAttribute("href", listTemplates[i].url);
            $(cloneItem).find(".mask a")[1].setAttribute("href", listTemplates[i].image_url);
            parentItem.append(cloneItem);
        }
        portfolioFilter.remove();
        portfolioItem.remove();
		$portfolio_selectors = $('.portfolio-filter >li>a');
		if($portfolio_selectors!='undefined'){
            parentItem.isotope({
				itemSelector : '.col-sm-3',
				layoutMode : 'fitRows'
			});
			
			$portfolio_selectors.on('click', function(){
				$portfolio_selectors.removeClass('active');
				$(this).addClass('active');
				var selector = $(this).attr('data-filter');
                parentItem.isotope({ filter: selector });
				return false;
			});
		}
        //Pretty Photo
        $("a[data-gallery^='prettyPhoto']").prettyPhoto({
            social_tools: false
        });
	});
	

	// Contact form validation
	var form = $('.contact-form');
	form.submit(function () {'use strict',
		$this = $(this);
		$.post($(this).attr('action'), function(data) {
			$this.prev().text(data.message).fadeIn().delay(3000).fadeOut();
		},'json');
		return false;
	});


	// Navigation Scroll
	$(window).scroll(function(event) {
		Scroll();
	});

	$('.navbar-collapse ul li a').click(function() {  
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 79}, 1000);
		return false;
	});

});

// Preloder script
jQuery(window).load(function(){'use strict';
	$(".preloader").delay(1600).fadeOut("slow").remove();
});

//Preloder script
jQuery(window).load(function(){'use strict';

	// Slider Height
	var slideHeight = $(window).height();
	$('#home .carousel-inner .item, #home .video-container').css('height',slideHeight);

	$(window).resize(function(){'use strict',
		$('#home .carousel-inner .item, #home .video-container').css('height',slideHeight);
	});

});


// User define function
function Scroll() {
	var contentTop      =   [];
	var contentBottom   =   [];
	var winTop      =   $(window).scrollTop();
	var rangeTop    =   200;
	var rangeBottom =   500;
	$('.navbar-collapse').find('.scroll a').each(function(){
		contentTop.push( $( $(this).attr('href') ).offset().top);
		contentBottom.push( $( $(this).attr('href') ).offset().top + $( $(this).attr('href') ).height() );
	});
	$.each( contentTop, function(i){
		if ( winTop > contentTop[i] - rangeTop ){
			$('.navbar-collapse li.scroll')
			.removeClass('active')
			.eq(i).addClass('active');			
		}
	})

}


	// Skill bar Function

jQuery(document).ready(function(){
	jQuery('.skillbar').each(function(){
		jQuery(this).find('.skillbar-bar').animate({
			width:jQuery(this).attr('data-percent')
		},6000);
	});
});


